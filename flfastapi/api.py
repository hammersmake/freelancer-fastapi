#!/usr/bin/env python
# coding: utf-8

# In[2]:


import freelancer


# In[2]:


from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI(title="Freelancer API",
    description="This is an API for Freelancer.com built at StartCOn2019",
    version="0.1")

class Searchjobs(BaseModel):
    namejob: str
    

@app.get("/searchjobs")
def searchjobs(namejobs):
    return freelancer.searchjobs(namejobs)


# In[ ]:


class Currencylookup(BaseModel):
    currency: str
    

@app.get("/currencylookup")
def currencylookup(currency):
    return freelancer.currencylookup(currency)


# In[ ]:


#class Createfixedpro(BaseModel):
#    title: str
#    description: str
#    budgetmin: int = None
#    budgetmax: int = None

#@app.post("/createfixedpro")
#def createfixedpro(title, description, budgetmin, budgetmax):
#    return freelancer.createfixedpro(title, description, budgetmin, budgetmax)


# In[ ]:





# In[ ]:


class zonetimes(BaseModel):
    zone: str
        
@app.get("/getzonetimes")
def getzonetimes():
    return freelancer.getzonetimes()


# In[ ]:


#createfixedpro('My new cool projects', 'this is a new project and i hope that it works. if it does not then that is ok.', 20, 159)


# In[1]:


class Getportfolo(BaseModel):
    usrid: str
    

@app.get("/getportfolo")
def getportfolios(usrid):
    return freelancer.getportfolios(usrid)

